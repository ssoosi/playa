<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'playa_db5');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wN.07yA?-J~U;SVG&3$^>JwNalgEuEw$KCjRC~J]k(`aIlMb!}A[<aD|TRa&>Qxm');
define('SECURE_AUTH_KEY',  'Xl_iihc<r.hl([<2o_pd(,t?.0&wAk_<C?SJt*>Qm[fr:d&CC[=8P+x.iO_]JZ2~');
define('LOGGED_IN_KEY',    'u@)ay:y;cK0~2]zK0,yxdO}K]:|CRqv0iMCxw|TwHmh6*W+.cC2$nP0Z..Kkf}J(');
define('NONCE_KEY',        '2WXh-$*;G4C+2!bIe>91O0DH9G%E8,I#s;YR[LciPw#P6ho}kv,Ktbi<{(gx2!=.');
define('AUTH_SALT',        '5-:8Jwm7@9a paoE9b-cZLHrbfpQ}^K0_kVsC*JL$>J*pkL`eR{@l.Sehzd2pQBu');
define('SECURE_AUTH_SALT', 'S7eCX*1vNmd5.S8SWx1yn>Ec8O>%-PmF/@vgTZx8E1@ =Z8{6fx6gB8[1u][AjsQ');
define('LOGGED_IN_SALT',   ']+YM.Ob`dd)H*3(h0gwyT0nW8p +><)yU)wylfJ)@(^M>r)ZbEuEm*^x%G8AP}bb');
define('NONCE_SALT',       'A6 iW9FTl=6#`whpV J*g]^+==yYAJno+#Q$F{GJb2hK`:i,# .B&P{af`k0qM,{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
